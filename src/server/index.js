import fs from 'fs';
import * as functions from './ipl.js';
import path from 'path';
import {fileURLToPath} from 'url';

const MATCHES_INTO_PATH = './../data/dataJSON/matches.json';
const DELIVERIES_NTO_PATH= './../data/dataJSON/deliveries.json';

const TOSS_AND_MATCH_OUTPUT_PATH = './../public/output/teamsWonTossAndMatches.json';
const PLAYER_OF_THE_MATCH_OUTPUT_PATH = './../public/output/playerOfTheMatchForEachSeason.json';
const STRIKE_RATE_OUTPUT_PATH = './../public/output/batsmanStrikeRateEachSeason.json';
const BEST_ECONOMY_OUTPUT_PATH = './../public/output/bestEconomyInSuperOvers.json';
const PLAYER_DISMISSED_OUTPUT_PATH = './../public/output/onePlayerDismissedByAnotherPlayer.json';
const RUNS_BY_VK_OUTPUT_PATH = './../public/output/runsScoredByViratKohli.json';
const TOTAL_BOUNDARIES_BY_MSD_OUTPUT_PATH = './../public/output/boundariesByMSD.json';

const _dirname = path.dirname(fileURLToPath(import.meta.url));

function readJSONFile(JSONFiles){
    return JSONFiles.map(file => {
        const resolvedPath = path.resolve(_dirname, file);
        const data = fs.readFileSync(resolvedPath, 'utf-8');
        return JSON.parse(data);
    });
}

const JSONFiles = [MATCHES_INTO_PATH, DELIVERIES_NTO_PATH];
const JSONFilesData = readJSONFile(JSONFiles);
const matches = JSONFilesData[0];
const deliveries = JSONFilesData[1];

function methodCallingAndSavingData(matches, deliveries){
    const resWonTossAndMatch = functions.teamsWonTossAndMatches(matches);
    const resPlayerOfTheMatch = functions.playerOfTheMatchForEachSeason(matches);
    const resStrikeRate = functions.batsmanStrikeRateEachSeason(matches, deliveries);
    const resPlayerDismissed = functions.onePlayerDismissedByAnotherPlayer(deliveries);
    const resBestEconomy = functions.bestEconomyInSuperOvers(deliveries);
    const resRunsByVK = functions.runsScoredByViratKohli(matches, deliveries); 
    const resTotalBoundariesByMSD = functions.boundariesByMSD(deliveries);

    const resultArray = [
        resWonTossAndMatch, 
        resPlayerOfTheMatch, 
        resStrikeRate, 
        resPlayerDismissed, 
        resBestEconomy, 
        resRunsByVK, 
        resTotalBoundariesByMSD
    ];
    const outputPathArray = [
        TOSS_AND_MATCH_OUTPUT_PATH, 
        PLAYER_OF_THE_MATCH_OUTPUT_PATH,
        STRIKE_RATE_OUTPUT_PATH, 
        PLAYER_DISMISSED_OUTPUT_PATH, 
        BEST_ECONOMY_OUTPUT_PATH,
        RUNS_BY_VK_OUTPUT_PATH,
        TOTAL_BOUNDARIES_BY_MSD_OUTPUT_PATH
    ];
    saveData(resultArray, outputPathArray);
}

methodCallingAndSavingData(matches, deliveries);

function saveData(resultArray, outputPathArray){
    let jsonData = {};
    let i = 0;
    resultArray.forEach(data => {
        for (const teams in data) {
            jsonData[teams] = data[teams];
        }
        const stringifyJSON = JSON.stringify(jsonData);
        const resolvedPath = path.resolve(_dirname, outputPathArray[i++]);
        fs.writeFileSync(resolvedPath, stringifyJSON);
        jsonData = {};
    });
}