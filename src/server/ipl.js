export function teamsWonTossAndMatches(matches) {
    const teamsArray = matches.filter(match => match.toss_winner === match.winner);
    return teamsArray.reduce((winnerTeams, match) => {
        const matchWinner = match.winner;
        if (winnerTeams[matchWinner]) {
            winnerTeams[matchWinner]++;
        } else {
            winnerTeams[matchWinner] = 1;
        }
        return winnerTeams;
    }, {});
}

export function playerOfTheMatchForEachSeason(matches) {
    const seasonArray = seasonCollection(matches);
    return seasonArray.reduce((playerOfTheMatch, year) => {
        const players = matches.reduce((winners, match) => {
            const season = match.season;
            if (year === season) {
                const potmWinner = match.player_of_match;
                if (winners[potmWinner]) {
                    winners[potmWinner]++;
                } else {
                    winners[potmWinner] = 1;
                }
            }
            return winners;
        }, {});
        const maxWinner = sortByValueAndReturnTop(players, 'desc');
        playerOfTheMatch[year] = maxWinner;
        return playerOfTheMatch;
    }, {});
}

export function batsmanStrikeRateEachSeason(matches, deliveries) {
    const seasonArray = seasonCollection(matches);
    return seasonArray.reduce((strikeRate, year) => {
        const matchIDArray = idCollection(matches, year);
        const runScored = {};
        const ballFaced = {};
        matchIDArray.forEach(id => {
            const delData = deliveries.filter(del => id === del.match_id && del.is_super_over === '0');
            delData.forEach(del => {
                const batsman = del.batsman;
                if (runScored[batsman]) {
                    runScored[batsman] += parseInt(del.batsman_runs);
                } else {
                    runScored[batsman] = parseInt(del.batsman_runs);
                }
                if (del.wide_runs === '0') {
                    if (ballFaced[batsman]) {
                        ballFaced[batsman]++;
                    } else {
                        ballFaced[batsman] = 1;
                    }
                }
            });
        });
        const batsmanStrikeRate = {};
        for (const [key, value] of Object.entries(runScored)) {
            if (batsmanStrikeRate[key]) {
                batsmanStrikeRate[key] += parseFloat(((value / ballFaced[key]) * 100).toFixed(2));
            } else {
                batsmanStrikeRate[key] = parseFloat(((value / ballFaced[key]) * 100).toFixed(2));
            }
        }
        const highestStrikeRate = sortByValueAndReturnTop(batsmanStrikeRate, 'desc');
        strikeRate[year] = highestStrikeRate;
        return strikeRate;
    }, {});
}

export function onePlayerDismissedByAnotherPlayer(deliveries) {
    const notRunOut = deliveries.filter(del => del.dismissal_kind !== 'run out');
    const batsmanNames = batsmanCollection(deliveries);
    const resultList = batsmanNames.reduce((list, batsman) => {
        const bowlersList = notRunOut.reduce((bowlers, del) => {
            const dismissedPlayer = del.player_dismissed;
            if (batsman === dismissedPlayer) {
                const bowlerName = del.bowler;
                if (bowlers[bowlerName]) {
                    bowlers[bowlerName]++;
                } else {
                    bowlers[bowlerName] = 1;
                }
            }
            return bowlers;
        }, {});
        if (Object.entries(bowlersList).length !== 0) {
            const maxNumber = sortByValueAndReturnTop(bowlersList, 'desc');
            list[batsman] = maxNumber;
        }
        return list;
    }, {});
    const combine = Object.values(resultList).reduce((previous, current) => {
        Object.keys(current).forEach((key) => previous[key] = previous[key] !== undefined ? Math.max(previous[key], current[key]) : current[key]);
        return previous;
    }, {});
    const result = {};
    const highest = sortByValueAndReturnTop(combine, 'desc');
    const batsman = Object.keys(resultList).find(player => JSON.stringify(resultList[player]) === JSON.stringify(highest));
    result[batsman] = highest;
    return result;
}

export function bestEconomyInSuperOvers(deliveries) {
    const superOverData = deliveries.filter(del => del.is_super_over !== '0');
    const totalRuns = {};
    const totalBalls = superOverData.reduce((balls, del) => {
        const bowler = del.bowler;
        if (totalRuns[bowler]) {
            totalRuns[bowler] += parseInt(del.total_runs) - (parseInt(del.legbye_runs) + parseInt(del.bye_runs));
        } else {
            totalRuns[bowler] = parseInt(del.total_runs) - (parseInt(del.legbye_runs) + parseInt(del.bye_runs));
        }
        if (del.wide_runs === '0' && del.noball_runs === '0') {
            if (balls[bowler]) {
                balls[bowler]++;
            } else {
                balls[bowler] = 1;
            }
        }
        return balls;
    }, {});
    const economy = {}
    for (let [bowler, runs] of Object.entries(totalRuns)) {
        economy[bowler] = parseFloat((runs / (totalBalls[bowler] / 6)).toFixed(2));
    }
    return sortByValueAndReturnTop(economy, 'asc');
}

export function runsScoredByViratKohli(matches, deliveries) {
    const vkData = deliveries.filter(del => del.batsman === 'V Kohli');
    const ids = idCollection(matches, '2016');
    return ids.reduce((totalRuns, id) => {
        const runsEachMatch = vkData.reduce((runs, del) => {
            const delID = del.match_id;
            if (id === delID) {
                runs += parseInt(del.batsman_runs);
            }
            return runs;
        }, 0);
        if (totalRuns['V Kohli']) {
            totalRuns['V Kohli'] += parseInt(runsEachMatch);
        } else {
            totalRuns['V Kohli'] = parseInt(runsEachMatch);
        }
        return totalRuns;
    }, {});
}

export function boundariesByMSD(deliveries) {
    const dataMSD = deliveries.filter(del => del.batsman === 'MS Dhoni');
    return dataMSD.reduce((totalBoundaries, del) => {
        const player = del.batsman;
        if (del.batsman_runs === '4' || del.batsman_runs === '6') {
            if (totalBoundaries[player]) {
                totalBoundaries[player]++;
            } else {
                totalBoundaries[player] = 1;
            }
        }
        return totalBoundaries;
    }, {});
}

// Helper functions

function seasonCollection(matches) {
    return matches.map(match => match.season)
        .filter((season, index, array) => array.indexOf(season) === index);
}

function sortByValueAndReturnTop(obj, order) {
    const top = Object.keys(obj).sort((a, b) => order === 'desc' ? obj[b] - obj[a] : obj[a] - obj[b])
    const topObj = {};
    topObj[top[0]] = obj[top[0]];
    return topObj;
}

function batsmanCollection(deliveries) {
    return deliveries.map(del => del.batsman)
        .filter((batsman, index, array) => array.indexOf(batsman) === index);
}

function idCollection(matches, year) {
    const seasonArr = seasonCollection(matches);
    if (seasonArr.includes(year)) {
        return matches.filter(match => match.season === year).map(matchID => matchID.id);
    } else if (year === 'All') {
        return matches.map(matchID => matchID.id);
    } else {
        return new Error('enter valid season or All');
    }
}