import csv from 'csvtojson'
import fs from 'fs';
import path from 'path';
import {fileURLToPath} from 'url';

const MATCHES_CSV_INPUT_PATH = './data/matches.csv';
const DELIVERIES_CSV_INPUT_PATH = './data/deliveries.csv';
const MATCHES_CSV_INTO_JSON = './data/dataJSON/matches.json';
const DELIVERIES_CSV_INTO_JSON = './data/dataJSON/deliveries.json';

const _dirname = path.dirname(fileURLToPath(import.meta.url));

const inputPathArray = [MATCHES_CSV_INPUT_PATH, DELIVERIES_CSV_INPUT_PATH];
const outputPathArray = [MATCHES_CSV_INTO_JSON, DELIVERIES_CSV_INTO_JSON];

function parsingCSVToJSON(inputPath, outputPath){
    let i = 0;
    inputPath.forEach(async (inputFile) => {
        const resolvedCSVPath = path.resolve(_dirname, inputFile);
        const jsonData = await csv().fromFile(resolvedCSVPath);
        const stringifyJSON = JSON.stringify(jsonData);
        const resolvedPath = path.resolve(_dirname, outputPath[i++]);
        fs.writeFileSync(resolvedPath, stringifyJSON);
    });
}

parsingCSVToJSON(inputPathArray, outputPathArray);